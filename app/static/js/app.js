$(document).ready(function () {
    eventHandlers();
    getSubscriberList()
});

function getSubscriberList() {
    $.ajax({
        'url' : '/subscriber',
        'method' : 'GET',
        'headers' : {'Content-Type' : 'application/json'},
        'dataType' : 'json',
        'success' : function (data, textStatus, jqXHR) {
            console.log(textStatus);
            displaySubscribers(data);
        },
        'error' : function (jqXHR, textStatus, error) {
            console.log('error');
            console.log(jqXHR);
            console.log(textStatus);
            console.log(error);
        }
    })
}

function displaySubscribers( subscribers ) {
    html = '<ul>';
    for ( subscriber in subscribers ) {
        if ( subscribers.hasOwnProperty( subscriber ) ) {
            html += '<li>' + subscriber + '</li>';
        }
    }
    html += '</ul>'
    $('#tabsubscribers').html(html)
    
    $('#tabsubscribers ul li').on('click', function () {
        $this = $(this);
        getUserDetails($this.text());
    })
}

function getUserDetails(userid){
    $.ajax({
        'url' : '/subscriber/' + userid,
        'method' : 'GET',
        'headers' : {'Content-Type' : 'application/json'},
        'dataType' : 'json',
        'success' : function (data, textStatus, jqXHR) {
            console.log(textStatus);
            displayUserDetails(data.data);
        },
        'error' : function (jqXHR, textStatus, error) {
            console.log('error');
            console.log(jqXHR);
            console.log(textStatus);
            console.log(error);
        }
    });
} 

function displayUserDetails( user ) {
    html = '<div id="selecteduserdetails">'
    html += '<div> Name : ' + user.name + '</div>';
    html += '<div> Address : ' + user.address + '</div>';
    html += '<div> DOB : ' + user.dob + '</div>';
    html += '<div>Subscriptions : <ul>'
    var subscriptions = user.subscriptions;
    subscriptions.forEach(function (subscription) {
        html += '<li>' + subscription.type + ' - ' + subscription.name + '</li>';
    })
    html += '</ul></div>'
    html += '</div>';
    
    $('#tabsubscribers div').remove();
    $('#tabsubscribers ul').after(html);
}

function eventHandlers() {
    $('[id^=mnu]').on('click', function () {
        $('[id^=tab]').hide();
        $this = $(this)
        tab = $this.attr('id').replace('mnu', 'tab');
        $('#' + tab).show();
    })
}