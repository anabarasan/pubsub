from flask import request, make_response
from models import Subscriber, Product, db, savedb

import json

def handleSubscriber(userid=None):
    if request.method == 'GET':
        return getSubscriber(userid)
    elif request.method == 'POST':
        return createSubscriber()
    elif request.method == 'PUT':
        return updateSubscriber()
    elif request.method == 'DELETE':
        return deleteSubscriber(userid)
    
def handleProduct():
    if request.method == 'GET':
        return getProducts()
    elif request.method == 'POST':
        return addProduct()
    
def getSubscriber(userid):
    if userid is None:
        return json.dumps(db['users'])
    
    export = request.args.get('export', 'json').strip().lower()
    subscriber = Subscriber({'name' : userid})
    
    if export not in['csv', 'json']:
        return json.dumps({'status' : 'error', 'message' : 'invalid export type'}) 
    elif export == 'csv':
        csv = '"name", "address", "dob", "subscription type", "subscription product"\n'
        details = subscriber.getDetails()
        subscriptions = details['subscriptions']
        if len(subscriptions) == 0:
            csv +='"%s","%s","%s","%s","%s"\n' % (details['name'], details['address'], details['dob'], '', '')
        for subscription in subscriptions:
            csv += '"%s","%s","%s","%s","%s"\n' % (details['name'], details['address'], details['dob'], subscription['type'], subscription['name'])
        
        response = make_response(csv)
        response.headers["Content-Disposition"] = "attachment; filename=export.csv"
        response.headers["Content-type"] = "text/csv"
        return response
    else:
        if not subscriber.existing:
            response = {"status" : "error", "message" : "Subscriber does not exist"}
        else:
            details = subscriber.getDetails()
            response = {"status" : "success", "data" : details}
        return json.dumps(response)
    
def createSubscriber():
    try:
        subscriber = Subscriber(request.json)
        if subscriber.existing:
            response = {"status" : "error", "message" : "Subscriber already exists"}
        else:
            subscriber.save()
            response = {"status" : "success", "message" : "Successfully added a Subscriber : %s" % (request.json['name'])}
    except:
        response = {"status" : "error", "message" : "Invalid Subscriber data"}
    return json.dumps(response)

def updateSubscriber():
    subscriber = Subscriber(request.json)
    if subscriber.existing:
        subscriber.update(request.json)
        response = {"status" : "success", "message" : "Subscriber data modified successfully : %s" % (request.json['name'])}
    else:
        response = {"status" : "error", "message" : "Subscriber does not exist"}
    return json.dumps(response)

def deleteSubscriber(userid):
    ret = Subscriber.delete({"name" : userid})
    if ret:
        response = {"status" : "success", "message" : "Subscriber successfully detleted for %s" % (userid)}
    else:
        response = {"status" : "error", "message" : "Subscriber does not exist"}
    return json.dumps(response)
    
def getProducts():
    return json.dumps(Product.getProducts())

def addProduct():
    product = Product(request.json)
    if product.existing:
        response = {"status" : "error", "message" : "Product already exists"}
    else:
        product.save()
        response = {"status" : "success", "message" : "Product %s added successfully." % (request.json['name'])}
    return json.dumps(response)
    
def productType():
    if request.method == 'GET':
        return json.dumps(sorted(db['products'].keys()))
    elif request.method == 'POST':
        pType = request.json['productType']
        if pType not in db['products'].keys():
            db['products'][pType] = []
            savedb(db)
            response = {"status" : "success", "message" : "Product added successfully"}
        else:
            response = {"status" : "error", "message" : "Product already exists"}
        return json.dumps(response)
    
def handleSubscription():
    data = request.json
    subscriber = Subscriber({'name' : data['subscriber']})
    if subscriber.existing:
        ret = subscriber.updateSubscription(data['productType'], data['name'])
        if ret == "Subscription successful":
            response = {"status" : "success", "message" : "User %s subscribed to Product %s" % (data['subscriber'], data['name'])}
        else:
            response = {"status" : "error", "message" : ret}
    else:
        response = {"status" : "error", "message" : "Subscriber does not exist"}
    return json.dumps(response)