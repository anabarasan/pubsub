import json
import os

APP_DB = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'db.json')

def loaddb():
    with open(APP_DB) as f:
        return json.load(f)

def savedb(mdb):
    with open(APP_DB, 'w') as f:
        json.dump(mdb, f, indent=4, sort_keys=True) 

db = loaddb()

class Subscriber(object):
    
    __user = None
    existing = False
    
    def __init__(self, user):
        self.__user = db['users'].get(user['name'].strip(), None)
        if self.__user is None:
            if user['name'].strip() != '':
                self.__user = { "name" : user['name'].strip(), "address" : user.get('address', '').strip(), "dob" : user.get('dob', '').strip() }
            else:
                raise Exception('Invalid Username')
        else:
            self.existing = True
        
    def save(self):
        if self.__user['address'].strip() != '' and self.__user['dob'].strip() != '':
            db['users'][self.__user['name']] = self.__user
            savedb(db)
        else:
            raise Exception('Invalid subscriber data')
        
    def update(self, user):
        userKeys = user.keys()
        if 'address' in userKeys:
            self.__user['address'] = user['address'].strip()
        if 'dob' in userKeys:
            self.__user['dob'] = user['dob'].strip()
        if 'address' in userKeys or 'dob' in userKeys:
            self.save()
            
    @classmethod
    def delete(cls, user):
        if db['users'].pop(user['name'], None) is not None:
            savedb(db)
            return True
        else:
            return False
            
    def getDetails(self):
        user = self.__user
        user['subscriptions'] = self.getSubscriptions()
        return user
    
    def getSubscriptions(self):
        return db['subscriptions'].get(self.__user['name'], [])
    
    def updateSubscription(self, productType, productName):
        if (productType in db['products'].keys() and productName in db['products'][productType]):
            subscriptions = self.getSubscriptions()
            subscription = {'type' : productType, 'name' : productName}
            if subscription in subscriptions:
                return "User %s already subscribed to Product %s" % (self.__user['name'], productName)
            else:
                subscriptions.append(subscription)
                db['subscriptions'][self.__user['name']] = subscriptions
                savedb(db)
                return "Subscription successful"
        return "Invalid subscription data"
        
class Product(object):
    
    __product = None
    existing = False
    
    def __init__(self, product):
        ptype = product['productType']
        if ptype in db['products'].keys():
            if product['name'] in db['products'][ptype]:
                #product exists
                self.existing = True
                self.__product = product
            else:
                #product does not exists
                db['products'][ptype].append(product['name'])
                self.__product = product
        else:
            raise Exception('Invalid product type')
        
    def save(self):
        savedb(db)
        
    @classmethod
    def getProducts(cls):
        productList = []
        products = db['products']
        for ptype, products in products.iteritems():
            productList.extend([{"type" : ptype, "product" : product} for product in products])
        return productList