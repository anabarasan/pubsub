from app import app
from flask import jsonify, render_template, request
from functools import wraps

import base64
import handlers

def check_auth(username, password):
    return username == 'admin' and password == 'secret'

def authenticate():
    message = {'message': "Unauthorized"}
    resp = jsonify(message)
    resp.status_code = 401
    return resp

def requires_auth(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        auth = request.headers.get('Authorization', None)
        if not auth:
            return authenticate()
        auth = base64.b64decode(auth).split(":")
        username, password = auth[0], auth[1]
        if not check_auth(username, password):
            return authenticate()
        return f(*args, **kwargs)
    return decorated

@app.route('/')
def home():
    return render_template('index.html')

@app.route('/subscriber', methods=['POST', 'GET'])
def createSubscribers():
    return handlers.handleSubscriber()

@app.route('/subscriber/<subscriberId>', methods=['GET', 'PUT', 'DELETE'])
def subscriberHandler(subscriberId):
    return handlers.handleSubscriber(subscriberId)

@app.route('/product', methods=['GET', 'POST'])
def productHandler():
    return handlers.handleProduct()

@app.route('/producttype', methods=['GET', 'POST'])
def handleProductType():
    return handlers.productType()

@app.route('/subscribe', methods=['POST'])
def subscriptionHandler():
    return handlers.handleSubscription()